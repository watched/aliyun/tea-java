[English](./README.md) | 简体中文

![](https://aliyunsdk-pages.alicdn.com/icons/AlibabaCloud.svg)

## Alibaba Cloud Tea for Java

[![Travis Build Status](https://travis-ci.org/aliyun/tea-java.svg?branch=master)](https://travis-ci.org/aliyun/tea-java)
[![Build status](https://ci.appveyor.com/api/projects/status/10g6aivxp0jwebr3?svg=true)](https://ci.appveyor.com/project/aliyun/tea-java)
[![codecov](https://codecov.io/gh/aliyun/tea-java/branch/master/graph/badge.svg)](https://codecov.io/gh/aliyun/tea-java)
[![Latest Stable Version](https://img.shields.io/maven-central/v/com.aliyun/tea.svg?label=Maven%20Central)](https://search.maven.org/search?q=g:%22com.aliyun%22%20AND%20a:%22tea%22)

## 安装

```xml
<dependency>
    <groupId>com.aliyun</groupId>
    <artifactId>tea</artifactId>
    <version>[1.2.0,2.0.0)</version>
</dependency>
```

## 问题
[提交 Issue](https://github.com/aliyun/tea-java/issues/new)，不符合指南的问题可能会立即关闭。

## 发行说明
每个版本的详细更改记录在[发行说明](./ChangeLog.txt)中。

## 相关
* [OpenAPI 开发者门户](https://next.api.aliyun.com/)
* [最新源码](https://github.com/aliyun/tea-java)

## 许可证
[Apache-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Copyright 2009-present Alibaba Cloud All rights reserved.

