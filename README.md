
![](https://aliyunsdk-pages.alicdn.com/icons/AlibabaCloud.svg)

## Alibaba Cloud Tea for Java

[![Travis Build Status](https://travis-ci.org/aliyun/tea-java.svg?branch=master)](https://travis-ci.org/aliyun/tea-java)
[![Build status](https://ci.appveyor.com/api/projects/status/10g6aivxp0jwebr3?svg=true)](https://ci.appveyor.com/project/aliyun/tea-java)
[![codecov](https://codecov.io/gh/aliyun/tea-java/branch/master/graph/badge.svg)](https://codecov.io/gh/aliyun/tea-java)
[![Latest Stable Version](https://img.shields.io/maven-central/v/com.aliyun/tea.svg?label=Maven%20Central)](https://search.maven.org/search?q=g:%22com.aliyun%22%20AND%20a:%22tea%22)

## Installation

```xml
<dependency>
    <groupId>com.aliyun</groupId>
    <artifactId>tea</artifactId>
    <version>[1.2.0,2.0.0)</version>
</dependency>
```

## Issues
[Opening an Issue](https://github.com/aliyun/tea-java/issues/new), Issues not conforming to the guidelines may be closed immediately.

## Changelog
Detailed changes for each release are documented in the [release notes](./ChangeLog.txt).

## References
* [OpenAPI Developer Portal](https://next.api.aliyun.com/)
* [Latest Release](https://github.com/aliyun/tea-java)

## License
[Apache-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Copyright 2009-present Alibaba Cloud All rights reserved.
